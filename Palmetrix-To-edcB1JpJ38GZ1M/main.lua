require('dateparse')

require('stringutil')

local msmq = require('msmq')

local function trace(a, b, c, d) return end

msmq.queue.PathName = '.\\PRIVATE$\\Patient.Service'

local Vmd = 'demo.vmd'

local JSON_TEMPLATE = [[
   {
      'PatientId':'',
      'LastName':'',
      'GivenName':'',
      'BirthPlace':'',
      'ContractCode':'',
      'ContractAmt':'',
      'Recorded':''
   }
]]

local XML_TEMPLATE = [[
<?xml version="1.0"?>
<AdmitPatient xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xmlns:xsd="http://www.w3.org/2001/XMLSchema"
        xmlns="http://tempuri.net/Patient.Messages">
	<PatientId></PatientId>
	<LastName></LastName>
	<GivenName></GivenName>
	<Race></Race>
	<PhoneHome></PhoneHome>
	<PhoneWork></PhoneWork>
	<Religion></Religion>
	<MaritalStatus></MaritalStatus>
	<Ssn></Ssn>
	<LicenseNo></LicenseNo>
	<Dob></Dob>
	<Sex></Sex>
	<BirthPlace></BirthPlace>
	<ContractCode></ContractCode>
	<ContractAmt></ContractAmt>
	<Recorded></Recorded>
</AdmitPatient>
]]

function MapPID(T, PID)
   T.Id            = PID[3][1][1]
   T.LastName      = PID[5][1][1][1]
   T.GivenName     = PID[5][1][2] 
   T.Race          = PID[10][1][1]
   T.PhoneHome     = PID[13][1][1]
   T.PhoneWork     = PID[14][1][1]
   T.Religion      = PID[17][1]
   T.MaritalStatus = PID[16][1]
   T.Ssn           = PID[19]
   T.LicenseNo     = PID[20][1]
   T.Dob           = PID[7][1]:DX()
   T.Sex           = PID[8]
   T.BirthPlace    = PID[23]
end

function MapXMLT(T)
   local XT = xml.parse{data = XML_TEMPLATE}
   XT.AdmitPatient.PatientId:setText(T.Id:S())
   if not T.LastName:isNull() then XT.AdmitPatient.LastName:setText(T.LastName:S()) end
   if not T.GivenName:isNull() then XT.AdmitPatient.GivenName:setText(T.GivenName:S()) end
   if not T.Race:isNull() then XT.AdmitPatient.Race:setText(T.Race:S()) end
   if not T.PhoneHome:isNull() then XT.AdmitPatient.PhoneHome:setText(T.PhoneHome:S()) end
   if not T.PhoneWork:isNull() then XT.AdmitPatient.PhoneWork:setText(T.PhoneWork:S()) end
   if not T.Religion:isNull() then XT.AdmitPatient.Religion:setText(T.Religion:S()) end
   if not T.MaritalStatus:isNull() then XT.AdmitPatient.MaritalStatus:setText(T.MaritalStatus:S()) end
   if not T.Ssn:isNull() then XT.AdmitPatient.Ssn:setText(T.Ssn:S()) end
   if not T.LicenseNo:isNull() then XT.AdmitPatient.LicenseNo:setText(T.LicenseNo:S()) end
   if not T.Dob:isNull() then XT.AdmitPatient.Dob:setText(T.Dob:S()) end
   if not T.Sex:isNull() then XT.AdmitPatient.Sex:setText(T.Sex:S()) end
   if not T.BirthPlace:isNull() then XT.AdmitPatient.BirthPlace:setText(T.BirthPlace:S()) end
   if not T.ContractCode:isNull() then XT.AdmitPatient.ContractCode:setText(T.ContractCode:S()) end
   if not T.ContractAmt:isNull() then XT.AdmitPatient.ContractAmt:setText(T.ContractAmt:S()) end
   if not T.Recorded:isNull() then XT.AdmitPatient.Recorded:setText(T.Recorded:S()) end
   return XT
end

function MapXML(T)
   local X = xml.parse{data = '<AdmitPatient></AdmitPatient>'}
   local Y = X.AdmitPatient
   Y:setAttr("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance")
   Y:setAttr("xmlns:xsd", "http://www.w3.org/2001/XMLSchema")
   Y:setAttr("xmlns", "http://tempuri.net/Patient.Messages")
   local Id = Y:append(xml.ELEMENT, 'PatientId')
   Id:setText(T.Id:S())
   local LastName = Y:append(xml.ELEMENT, 'LastName')
   if not T.LastName:isNull() then LastName:setText(T.LastName:S()) end
   local GivenName = Y:append(xml.ELEMENT, 'GivenName')
   if not T.GivenName:isNull() then GivenName:setText(T.GivenName:S()) end
   local Race = Y:append(xml.ELEMENT, 'Race')
   if not T.Race:isNull() then Race:setText(T.Race:S()) end
   local PhoneHome = Y:append(xml.ELEMENT, 'PhoneHome')
   if not T.PhoneHome:isNull() then PhoneHome:setText(T.PhoneHome:S()) end
   local PhoneWork = Y:append(xml.ELEMENT, 'PhoneWork')
   if not T.PhoneWork:isNull() then PhoneWork:setText(T.PhoneWork:S()) end
   local Religion = Y:append(xml.ELEMENT, 'Religion')
   if not T.Religion:isNull() then Religion:setText(T.Religion:S()) end
   local MaritalStatus = Y:append(xml.ELEMENT, 'MaritalStatus')
   if not T.MaritalStatus:isNull() then MaritalStatus:setText(T.MaritalStatus:S()) end
   local Ssn = Y:append(xml.ELEMENT, 'Ssn')
   if not T.Ssn:isNull() then Ssn:setText(T.Ssn:S()) end
   local LicenseNo = Y:append(xml.ELEMENT, 'LicenseNo')
   if not T.LicenseNo:isNull() then LicenseNo:setText(T.LicenseNo:S()) end
   local Dob = Y:append(xml.ELEMENT, 'Dob')
   if not T.Dob:isNull() then Dob:setText(T.Dob:S()) end
   local Sex = Y:append(xml.ELEMENT, 'Sex')
   if not T.Sex:isNull() then Sex:setText(T.Sex:S()) end
   local BirthPlace = Y:append(xml.ELEMENT, 'BirthPlace')
   if not T.BirthPlace:isNull() then BirthPlace:setText(T.BirthPlace:S()) end
   local ContractCode = Y:append(xml.ELEMENT, 'ContractCode')
   if not T.ContractCode:isNull() then ContractCode:setText(T.ContractCode:S()) end
   local ContractAmt = Y:append(xml.ELEMENT, 'ContractAmt')
   if not T.ContractAmt:isNull() then ContractAmt:setText(T.ContractAmt:S()) end
   local Recorded = Y:append(xml.ELEMENT, 'Recorded')
   if not T.Recorded:isNull() then Recorded:setText(T.Recorded:S()) end
   return X
end

function MapPV1(T, PV1)
   T.ContractCode = PV1[24][1]
   T.ContractAmt = PV1[26][1]
end

function MapEVN(T, EVN)
   T.Recorded = EVN[2][1]:TX()
end

function node.setText(X, T)
   X:text():setInner(T)
end

function node.text(X)
   for i = 1, #X do
      if X[i]:nodeType() == 'text' then
         return X[i]
      end
   end
   return X:append(xml.TEXT, '')
end

function node.setAttr(N, K, V)
   if N:nodeType() ~= 'element' then
      error('Must be an element')
   end
   if not N[K] or N[K]:nodeType() ~= 'attribute' then
      N:append(xml.ATTRIBUTE, K)
   end
   N[K] = V
   return N
end

local function splitBatch(Data)
   -- strip batch info off Data
   local a = Data:split('\r')
   Data = a[3]
   for i = 4, #a-2 do
      Data = Data .. '\r' .. a[i]
   end

   -- split Data into messages
   local delimiter = 'MSH|^~\\&|'
   local b = Data:split(delimiter)

   -- add MSH segment info
   for i = 2, #b do
      b[i-1] = delimiter .. b[i]
   end
   b[#b] = nil

   -- global variable to count messages
   MsgCnt = #b

   -- globals for batch segment info
   FHS = a[1]:split('|')
   BHS = a[2]:split('|')
   FTS = a[#a]:split('|')
   BTS = a[#a-1]:split('|')

   return b
end

local function MapData(Msg)
   local H, Name = hl7.parse{vmd = Vmd, data = Msg}
   local Out = db.tables{vmd = Vmd, name = Name}
   return Out
end

function MapJSON(T)
   local J = json.parse{data = JSON_TEMPLATE}
   if not T.Id:isNull() then J.PatientId = T.Id:S() end
   if not T.LastName:isNull() then J.LastName = T.LastName:S() end
   if not T.GivenName:isNull() then J.GivenName = T.GivenName:S() end
   if not T.BirthPlace:isNull() then J.BirthPlace = T.BirthPlace:S() end
   if not T.ContractCode:isNull() then J.ContractCode = T.ContractCode:S() end
   if not T.ContractAmt:isNull() then J.ContractAmt = T.ContractAmt:S() end
   if not T.Recorded:isNull() then J.Recorded = T.Recorded:S() end
   return J
end

function processMsg(Msg)
   local H, MsgType = hl7.parse{vmd = Vmd, data = Msg}
   local MsgMap = MapMsg(H, MsgType)
   local M = hl7.parse{vmd = Vmd, data = MsgMap}
   local Tables = MapData(MsgMap)

   if MsgType == 'ADT' then
      MapPID(Tables.Patient[1], M.PID)
      MapPV1(Tables.Patient[1], M.PV1)
      MapEVN(Tables.Patient[1], M.EVN)
      
      local XT = MapXMLT(Tables.Patient[1])
      local SerializedXmlT = XT:S()

      local X = MapXML(Tables.Patient[1])
      local SerializedXml = X:S()
      
      local J = MapJSON(Tables.Patient[1])
      local SerializedJsonF = json.serialize{data = J, compact = false}
      local SerializedJson = json.serialize{data = J, compact = true}

      if not iguana.isTest() then
         msmq.send(SerializedXmlT, 1)
      end
   end
   trace(MsgType)

   return
end

local function convertTerminators(Data)
   Data = Data:gsub('\r\n','\r')
   return Data:gsub('\n','\r')
end

function MapMsg(MsgIn, MsgType)
   local MsgOut = hl7.message{vmd = 'demo.vmd', name = MsgType}
   MsgOut:mapTree(MsgIn)
   MsgOut.MSH[3][1] = MsgIn.MSH[5][1]
   MsgOut.MSH[4][1] = MsgIn.MSH[6][1]
   MsgOut.MSH[5][1] = MsgIn.MSH[3][1]
   MsgOut.MSH[6][1] = MsgIn.MSH[4][1]
   if MsgType == 'ADT' then
      local Sex = MsgOut.PID[8]:nodeValue()
      MsgOut.PID[8] = Sex:upper()
      Sex = Sex:lower()
      if Sex == 'female' then
         MsgOut.PID[8] = 'F'
      elseif Sex == 'male' then
         MsgOut.PID[8] = 'M'
      elseif Sex == 'other' then
         MsgOut.PID[8] = 'O'
      elseif Sex ~= 'f' and Sex ~= 'm' and Sex ~= 'o' then
         MsgOut.PID[8] = 'U'
      end
   end
   return MsgOut:S()
end

function main(Data)
   -- convert non-conformant terminators to "\r"
   Data = convertTerminators(Data)

   -- split batch into array of messages
   local a = Data:split('\r')
   local b = a[1]:split('|')
   local Msg = Data
   if b[1] == 'FHS' then
      Msgs = splitBatch(Data)
      -- process messages
      for i = 1, #Msgs do
         processMsg(Msgs[i])
      end
   else
      processMsg(Msg)
   end

   return
end