-- Trims white space on both sides.
function string.trimWS(self)
   return self:match('^%s*(.-)%s*$')
end

-- Trims white space on right side.
function string.trimRWS(self)
   return self:match('^(.-)%s*$')
end

-- Trims white space on left side.
function string.trimLWS(self)
   return self:match('^%s*(.-)$')
end

-- This routine will replace multiple spaces with single spaces 
function string.compactWS(self) 
   return self:gsub("%s+", " ") 
end

-- This routine capitalizes the first letter of the string
-- and returns the rest in lower characters
function string.capitalize(self)
   local R = self:sub(1,1):upper()..self:sub(2):lower()
   return R
end

function string.split(s, d)
   local t = {}
   local i = 0
   local f
   local match = '(.-)' .. d .. '()'
   if string.find(s, d) == nil then
      return {s}
   end
   for sub, j in string.gfind(s, match) do
      i = i + 1
      t[i] = sub
      f = j
   end
   if i~= 0 then
      t[i+1]=string.sub(s,f)
   end
   return t
end