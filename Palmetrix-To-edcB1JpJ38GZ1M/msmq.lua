local luacom = require('luacom')

local function trace(a,b,c,d) return end

local msmq = {}

msmq.queue = {
   PathName = '.\\PRIVATE$\\845f119e-b8de-490d-8165-144e5e99a404',
}

local function ExportConstants()
   local const = {}
   local QInfo = luacom.CreateObject('MSMQ.MSMQQueueInfo.1')
   assert(QInfo)

   luacom.ExportConstants(QInfo, const)
   return const
end

msmq.const = ExportConstants()

function msmq.send(Data, isTransactional)
   local destinationQueue = luacom.CreateObject("MSMQ.MSMQDestination")
	local message = luacom.CreateObject("MSMQ.MSMQMessage")
	
	transactionLevel = msmq.const.MQ_NO_TRANSACTION
	if isTransactional then
      transactionLevel = msmq.const.MQ_SINGLE_MESSAGE
   end
      
	local formatName = "DIRECT=OS:" .. msmq.queue.PathName
	destinationQueue.FormatName = formatName
   trace(destinationQueue.FormatName)

	message.Body = Data
	message:Send(destinationQueue, transactionLevel)
end

return msmq